/**
 *
 */
package com.psystems.buku.api.controller;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.psystems.buku.model.Category;
import com.psystems.buku.model.ContentCreator;
import com.psystems.buku.model.ContentCreatorType;
import com.psystems.buku.model.Media;
import com.psystems.buku.model.MediaFormat;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Set;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author rafaelpevidor
 *
 */
@SpringBootTest //is useful when we need to bootstrap the entire container.
@AutoConfigureMockMvc
@DatabaseSetup("/dbunit/data/media_data.xml")
@TestExecutionListeners({
	TransactionalTestExecutionListener.class,
	DependencyInjectionTestExecutionListener.class,
	DbUnitTestExecutionListener.class
})
public class MediaRestControllerTest extends AbstractRestControllerTest {

	//@Autowired
	private MockMvc mvc;

	private Category category;
	private ContentCreatorType type;
	private ContentCreator contentCreator;
	private MediaFormat format;

	//@BeforeEach
	void init() {

	}

	//@Test
	void whenCreatingMedia_ShouldReturnCreatedAsStatus() throws Exception {
		Category category = new Category(1l, "Ação", "Ação");
		ContentCreatorType type = new ContentCreatorType(1L, "Autor/Escritor", "Escritor");
		ContentCreator contentCreator = new ContentCreator(1L, "George", "R. R. Martin",
				"É mais conhecido por escrever a série de livros de fantasia épica As Crônicas de Gelo e Fogo.", type);
		MediaFormat format = MediaFormat.builder().id(2l).build();
				//new MediaFormat(2l, "E-book");
		Media media = Media.builder().title("Dança dos dragões")
				.type(com.psystems.buku.model.MediaType.DIGITAL)
				.categories(Set.of(category))
				.format(format)
				.contentCreators(Set.of(contentCreator)).build();
		this.mvc.perform(post("/api/medias")
				.content(asJsonString(media))
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isCreated());
	}

	//@Test
	void whenUpdatingMedia_ShouldReturnOkAsStatus() throws Exception {
		Category category = new Category(1l, "Ação", "Ação");
		ContentCreatorType type = new ContentCreatorType(1L, "Autor/Escritor", "Escritor");
		ContentCreator contentCreator = new ContentCreator(1L, "George", "R. R. Martin",
				"É mais conhecido por escrever a série de livros de fantasia épica As Crônicas de Gelo e Fogo.", type);
		MediaFormat format = MediaFormat.builder().id(2l).name("E-book").build();
		Media media = Media.builder().title("Dança dos dragões")
				.type(com.psystems.buku.model.MediaType.DIGITAL)
				.categories(Set.of(category))
				.format(format)
				.contentCreators(Set.of(contentCreator)).build();
		this.mvc.perform(put("/api/medias/{id}", 2)
				.content(asJsonString(media))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.title").exists())
		.andExpect(jsonPath("$.title").value("Dança dos dragões"));
	}

	//@Test
	void shouldReturnAllMedias() throws Exception {
		mvc.perform(get("/api/medias")
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.content").exists())
		.andExpect(jsonPath("$.content[*].id").isNotEmpty())
		.andExpect(jsonPath("$.totalElements").exists())
		.andExpect(jsonPath("$.totalElements").value(2))
		.andExpect(jsonPath("$.stakeholders").doesNotExist())
		.andExpect(jsonPath("$.categories").doesNotExist());
	}

	//@Test
	void shouldReturnMediaById() throws Exception {
		mvc.perform(get("/api/medias/{id}", 2)
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.title").exists())
		.andExpect(jsonPath("$.title").value("Tormenta de espadas"));
	}

	//@Test
	void whenDeletingMedia_ShouldReturnOkAsStatus() throws Exception {
		this.mvc.perform(delete("/api/medias/{id}", 1)).andExpect(status().is(204));
	}
}
