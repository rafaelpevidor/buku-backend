/**
 *
 */
package com.psystems.buku.api.controller;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.psystems.buku.model.MediaFormat;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author rafaelpevidor
 *
 */
@SpringBootTest
@AutoConfigureMockMvc
@DatabaseSetup("/dbunit/data/mediaformat_data.xml")
@TestExecutionListeners({
	TransactionalTestExecutionListener.class,
	DependencyInjectionTestExecutionListener.class,
	DbUnitTestExecutionListener.class
})
public class MediaFormatRestControllerTest extends AbstractRestControllerTest {

	//@Autowired
	private MockMvc mvc;

	//@Test
	public void whenCreatingMediaFormatShouldReturnCreatedAsStatus() throws Exception {
		this.mvc.perform(post("/api/mediasformats")
				.content(asJsonString(MediaFormat.builder().name("Novela").build()))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isCreated());
	}

	//@Test
	public void whenCreatingMediaFormatWithoutRequiredFields_ShouldReturnBadRequestAsStatus() throws Exception {
		this.mvc.perform(post("/api/mediasformats")
				.content(asJsonString(MediaFormat.builder().name(" ").build()))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isBadRequest());
	}

	//@Test
	public void whenCreatingMediaFormatWithInvalidNameRequiredFields_ShouldReturnBadRequestAsStatus() throws Exception {
		//name less than 3 characters
		this.mvc.perform(post("/api/mediasformats")
				.content(asJsonString(MediaFormat.builder().name("A").build()))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isBadRequest());

		//name with 60 characters
		this.mvc.perform(post("/api/mediasformats")
				.content(asJsonString(MediaFormat.builder().name("012345678901234567890123456789012345678901234567890123456789").build()))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isBadRequest());
	}

	//@Test
	public void shouldReturnAllMediasFormats() throws Exception {
		mvc.perform(get("/api/mediasformats")
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.content").exists())
		.andExpect(jsonPath("$.content[*].id").isNotEmpty())
		.andExpect(jsonPath("$.totalElements").exists())
		.andExpect(jsonPath("$.totalElements").value(17));
	}

	//@Test
	public void shouldReturnLastPageOfMediasFormats() throws Exception {
		mvc.perform(get("/api/mediasformats").queryParam("page", "3").queryParam("size", "5")
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.content").exists())
		.andExpect(jsonPath("$.content[*].id").isNotEmpty())
		.andExpect(jsonPath("$.last").exists())
		.andExpect(jsonPath("$.last").value(true))
		.andExpect(jsonPath("$.totalPages").exists())
		.andExpect(jsonPath("$.totalPages").value(4))
		.andExpect(jsonPath("$.numberOfElements").exists())
		.andExpect(jsonPath("$.numberOfElements").value(2))
		.andExpect(jsonPath("$.totalElements").exists())
		.andExpect(jsonPath("$.totalElements").value(17));
	}

	//@Test
	public void whenUpdatingMediaFormatShouldReturnOkAsStatus() throws Exception {
		this.mvc.perform(put("/api/mediasformats/{id}", 7)
				.content(asJsonString(MediaFormat.builder().id(7l).name("Livro Digital").build()))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.name").exists())
		.andExpect(jsonPath("$.name").value("Livro Digital"));
	}

	//@Test
	public void shouldReturnMediaFormatyById() throws Exception {
		mvc.perform(get("/api/mediasformats/{id}", 4)
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.name").exists())
		.andExpect(jsonPath("$.name").value("Curso"));
	}

	//@Test
	public void whenDeletingMediaFormatShouldReturnOkAsStatus() throws Exception {
		this.mvc.perform(delete("/api/mediasformats/{id}", 3)).andExpect(status().is(204));
	}
}
