/**
 *
 */
package com.psystems.buku.api.controller;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.psystems.buku.model.Category;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author rafaelpevidor
 *
 */
@SpringBootTest
@AutoConfigureMockMvc
@DatabaseSetup("/dbunit/data/category_data.xml")
@TestExecutionListeners({
	TransactionalTestExecutionListener.class,
	DependencyInjectionTestExecutionListener.class,
	DbUnitTestExecutionListener.class
})
public class CategoryRestControllerTest extends AbstractRestControllerTest {

	//@Autowired
	private MockMvc mvc;

	//@Test
	public void whenCreatingCategoryShouldReturnCreatedAsStatus() throws Exception {
		this.mvc.perform(post("/api/categories")
				.content(asJsonString(Category.builder().name("Ficção Científica").build()))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isCreated());
	}

	//@Test
	public void whenCreatingCategoryWithoutRequiredFields_ShouldReturnBadRequestAsStatus() throws Exception {
		this.mvc.perform(post("/api/categories")
				.content(asJsonString(Category.builder().name(" ").build()))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isBadRequest());
	}

	//@Test
	public void whenCreatingCategoryWithInvalidNameRequiredFields_ShouldReturnBadRequestAsStatus() throws Exception {
		//name less than 3 characters
		this.mvc.perform(post("/api/categories")
				.content(asJsonString(Category.builder().name("A").build()))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isBadRequest());

		//name with 60 characters
		this.mvc.perform(post("/api/categories")
				.content(asJsonString(Category.builder().name("012345678901234567890123456789012345678901234567890123456789")))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isBadRequest());

		//description greater than 600
		Category ca = Category.builder().name("teste").build();
		ca.setDescription("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent euismod dolor nec blandit condimentum. "
				+ "Etiam posuere, nulla ac eleifend venenatis, augue risus sagittis lacus, id auctor nisi tellus nec tortor. Fusce in feugiat ligula. "
				+ "Donec in arcu sed purus pellentesque mollis ut nec nisl. Morbi vestibulum lacus eget sapien ultricies accumsan. "
				+ "Nam mauris magna, accumsan non ipsum nec, feugiat elementum dolor. Curabitur luctus ut nisi a varius. Nunc aliquam arcu ut tellus fermentum, "
				+ "in rutrum ante porttitor. Maecenas iaculis consectetur ex vel sagittis. Nulla facilisi. Pellentesque aliquet vitae lorem sit amet volutpat. "
				+ "Aenean tempus risus id leo tincidunt suscipit. Aenean.");
		this.mvc.perform(post("/api/categories")
				.content(asJsonString(ca))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isBadRequest());
	}

	//@Test
	public void shouldReturnAllCategories() throws Exception {
		mvc.perform(get("/api/categories")
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.content").exists())
		.andExpect(jsonPath("$.content[*].id").isNotEmpty())
		.andExpect(jsonPath("$.totalElements").exists())
		.andExpect(jsonPath("$.totalElements").value(4));
	}

	//@Test
	public void whenUpdatingCategory_ShouldReturnOkAsStatus() throws Exception {
		this.mvc.perform(put("/api/categories/{id}", 3)
				.content(asJsonString(new Category(3l, "Policial", null)))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.name").exists())
		.andExpect(jsonPath("$.name").value("Policial"));
	}

	//@Test
	public void shouldReturnCategoryById() throws Exception {
		mvc.perform(get("/api/categories/{id}", 4)
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.name").exists())
		.andExpect(jsonPath("$.name").value("Suspense"))
		.andExpect(jsonPath("$.description").exists())
		.andExpect(jsonPath("$.description").value("Suspense"));
	}

	//@Test
	public void whenDeletingCategory_ShouldReturnOkAsStatus() throws Exception {
		this.mvc.perform(delete("/api/categories/{id}", 3)).andExpect(status().is(204));
	}
}
