/**
 *
 */
package com.psystems.buku.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author rafaelpevidor
 *
 */
public abstract class AbstractRestControllerTest {

	protected static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
}
