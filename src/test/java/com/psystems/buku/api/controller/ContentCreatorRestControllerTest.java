/**
 *
 */
package com.psystems.buku.api.controller;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.psystems.buku.model.ContentCreator;
import com.psystems.buku.model.ContentCreatorType;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author rafaelpevidor
 *
 */
@SpringBootTest
@AutoConfigureMockMvc
@DatabaseSetup("/dbunit/data/creator_data.xml")
@TestExecutionListeners({
	TransactionalTestExecutionListener.class,
	DependencyInjectionTestExecutionListener.class,
	DbUnitTestExecutionListener.class
})
public class ContentCreatorRestControllerTest extends AbstractRestControllerTest {

	//@Autowired
	private MockMvc mvc;

	//@Test
	void whenCreatingContentCreator_shouldReturnCreatedAsStatus() throws Exception {
		this.mvc.perform(post("/api/content-creators")
				.content(asJsonString(ContentCreator.builder().firstName("J. K.").lastName("Rowling").type(getCreatorType()).build()))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isCreated());
	}

	//@Test
	void shouldReturnAllContentCreators() throws Exception {
		mvc.perform(get("/api/content-creators")
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.content").exists())
		.andExpect(jsonPath("$.content[*].id").isNotEmpty())
		.andExpect(jsonPath("$.totalElements").exists())
		.andExpect(jsonPath("$.totalElements").value(2));
	}

	//@Test
	void whenUpdatingContentCreator_shouldReturnOkAsStatus() throws Exception {
		this.mvc.perform(put("/api/content-creators/{id}", 2)
				.content(asJsonString(new ContentCreator(2l, "David", "Allen", null, getCreatorType())))
				.contentType(MediaType.APPLICATION_JSON)).andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.firstName").exists())
		.andExpect(jsonPath("$.firstName").value("David"));
	}

	private ContentCreatorType getCreatorType() {
		return new ContentCreatorType(1L, "Autor", null);
	}

	//@Test
	void shouldReturnContentCreatorById() throws Exception {
		mvc.perform(get("/api/content-creators/{id}", 2)
				.accept(MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.firstName").exists())
		.andExpect(jsonPath("$.firstName").value("Augusto"))
		.andExpect(jsonPath("$.lastName").exists())
		.andExpect(jsonPath("$.lastName").value("Cury"));
	}

	//@Test
	void whenDeleteAcreatorShouldReturnOkAsStatus() throws Exception {
		this.mvc.perform(delete("/api/content-creators/{id}", 1)).andExpect(status().is(204));
	}
}
