package com.psystems.buku;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.psystems.buku.api.controller.CategoryRestController;

@SpringBootTest
class BukuApplicationTests {

	@Autowired
	private CategoryRestController categoryController;

	@Test
	void contextLoads() {
		assertThat(categoryController).isNotNull();
	}

}
