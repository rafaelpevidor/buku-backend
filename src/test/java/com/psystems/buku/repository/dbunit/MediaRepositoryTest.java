/**
 *
 */
package com.psystems.buku.repository.dbunit;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.psystems.buku.model.Category;
import com.psystems.buku.model.ContentCreator;
import com.psystems.buku.model.Media;
import com.psystems.buku.model.MediaFormat;
import com.psystems.buku.model.MediaType;
import com.psystems.buku.repository.CategoryRepository;
import com.psystems.buku.repository.ContentCreatorRepository;
import com.psystems.buku.repository.MediaFormatRepository;
import com.psystems.buku.repository.MediaRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author rafaelpevidor
 *
 */
@DataJpaTest //provides some standard setup needed for testing the persistence layer
@DatabaseSetup("/dbunit/data/media_data.xml")
@TestExecutionListeners({
		DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class
})
class MediaRepositoryTest {

	@Autowired
	private MediaRepository repository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private ContentCreatorRepository contentCreatorRepository;

	@Autowired
	private MediaFormatRepository mediaFormatRepository;

	@Test
	void whenValidFields_shouldReturnCreatedMedia() {
		Category category = categoryRepository.findById(1l).orElse(new Category());
		ContentCreator contentCreator = contentCreatorRepository.findById(1l).orElse(new ContentCreator());
		MediaFormat format = mediaFormatRepository.findById(1l).orElse(new MediaFormat());
		Media media = Media.builder().title("Guerreiro").type(MediaType.DIGITAL).categories(Set.of(category)).format(format).contentCreators(Set.of(contentCreator)).build();
		repository.save(media);
		assertThat(media).isNotNull().hasFieldOrPropertyWithValue("id", 3L);
	}
}
