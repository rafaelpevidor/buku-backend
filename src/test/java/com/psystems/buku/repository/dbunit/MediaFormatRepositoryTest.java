package com.psystems.buku.repository.dbunit;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.psystems.buku.model.MediaFormat;
import com.psystems.buku.repository.MediaFormatRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest //provides some standard setup needed for testing the persistence layer
@DatabaseSetup("/dbunit/data/mediaformat_data.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
public class MediaFormatRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private MediaFormatRepository repository;

    @Test
    @Override
    void given_validFields_whenSave_shouldReturnCreatedEntity() {
        MediaFormat mediaFormat = MediaFormat.builder().name("Custom").description("Custom").build();
        assertThat(repository.save(mediaFormat)).isNotNull()
                .hasNoNullFieldsOrProperties()
                .hasFieldOrPropertyWithValue("name", "Custom")
                .hasFieldOrPropertyWithValue("description", "Custom");
    }

    @Test
    @Override
    void givenId_shouldReturnEntity() {
        //given
        Long id = 1l;

        //when
        var expected = MediaFormat.builder().id(1l).name("Blu-ray")
                .build();
        var category = repository.findById(id);

        //then
        assertThat(category).isPresent().hasValue(expected);
    }

    @Test
    @Override
    void givenId_whenUpdate_shouldReturnUpdatedEntity() {
        //given
        Long id = 2l;

        //when
        var mediaFormat = repository.findById(id).orElse(new MediaFormat());
        assertThat(mediaFormat).hasFieldOrPropertyWithValue("name", "CD/DVD");

        mediaFormat.setDescription("Old Style Format");

        //then
        assertThat(repository.save(mediaFormat))
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .hasFieldOrPropertyWithValue("description", "Old Style Format");
    }

    @Test
    @Override
    void givenId_shouldDeleteEntity() {
        //given
        Long id = 3l;

        //when
        repository.deleteById(id);

        //then
        assertThat(repository.findById(id)).isEmpty();
    }

    @Test
    @Override
    void shouldListAllEntities() {
        var mediaFormats = repository.findAll();
        assertThat(mediaFormats).isNotEmpty().hasSize(9);
    }
}
