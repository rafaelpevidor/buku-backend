package com.psystems.buku.repository.dbunit;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.psystems.buku.model.Category;
import com.psystems.buku.repository.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest //provides some standard setup needed for testing the persistence layer
@DatabaseSetup("/dbunit/data/category_data.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
class CategoryRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private CategoryRepository repository;

    @Test
    @Override
    void given_validFields_whenSave_shouldReturnCreatedEntity() {
        Category category = Category.builder().name("Custom").description("Custom Category").build();
        assertThat(repository.save(category)).isNotNull()
                .hasNoNullFieldsOrProperties()
                .hasFieldOrPropertyWithValue("name", "Custom");
    }

    @Test
    @Override
    void givenId_shouldReturnEntity() {
        //given
        Long id = 1l;

        //when
        var expected = Category.builder().id(1l).name("Ação").description("Ação").build();
        var category = repository.findById(id);

        //then
        assertThat(category).isPresent().hasValue(expected);
    }

    @Test
    @Override
    void givenId_whenUpdate_shouldReturnUpdatedEntity() {
        //given
        Long id = 2l;

        //when
        var category = repository.findById(id).orElse(new Category());
        assertThat(category).hasNoNullFieldsOrProperties();

        category.setName("Adventure");
        category.setDescription("Adventure");

        //then
        assertThat(repository.save(category))
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .hasFieldOrPropertyWithValue("name", "Adventure")
                .hasFieldOrPropertyWithValue("description", "Adventure");
    }

    @Test
    @Override
    void givenId_shouldDeleteEntity() {
        //given
        Long id = 3l;

        //when
        repository.deleteById(id);

        //then
        assertThat(repository.findById(id)).isEmpty();
    }

    @Test
    @Override
    void shouldListAllEntities() {
        var categories = repository.findAll();
        assertThat(categories).isNotEmpty().hasSize(4);
    }
}
