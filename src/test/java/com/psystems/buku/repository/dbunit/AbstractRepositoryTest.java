package com.psystems.buku.repository.dbunit;

public abstract class AbstractRepositoryTest {

    abstract void given_validFields_whenSave_shouldReturnCreatedEntity();
    abstract void givenId_shouldReturnEntity();
    abstract void givenId_whenUpdate_shouldReturnUpdatedEntity();
    abstract void givenId_shouldDeleteEntity();
    abstract void shouldListAllEntities();
}
