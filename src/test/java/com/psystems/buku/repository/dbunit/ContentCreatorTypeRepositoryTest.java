package com.psystems.buku.repository.dbunit;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.psystems.buku.model.ContentCreatorType;
import com.psystems.buku.repository.ContentCreatorTypeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest //provides some standard setup needed for testing the persistence layer
@DatabaseSetup("/dbunit/data/creatortype_data.xml")
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DbUnitTestExecutionListener.class
})
public class ContentCreatorTypeRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private ContentCreatorTypeRepository repository;

    @Test
    void given_validFields_whenSave_shouldReturnCreatedEntity() {
        ContentCreatorType contentCreatorType = ContentCreatorType.builder()
                .name("Custom")
                .description("Custom")
                .build();
        assertThat(repository.save(contentCreatorType)).isNotNull()
                .hasNoNullFieldsOrProperties()
                .hasFieldOrPropertyWithValue("name", "Custom")
                .hasFieldOrPropertyWithValue("description", "Custom");
    }

    @Test
    @Override
    void givenId_shouldReturnEntity() {
        //given
        Long id = 1l;

        //when
        var expected = ContentCreatorType.builder().id(1l).name("Ator/Atriz")
                .description("Pessoa que atua/interpreta")
                .build();
        var category = repository.findById(id);

        //then
        assertThat(category).isPresent().hasValue(expected);
    }

    @Test
    @Override
    void givenId_whenUpdate_shouldReturnUpdatedEntity() {
        //given
        Long id = 2l;

        //when
        var contentCreatorType = repository.findById(id).orElse(new ContentCreatorType());
        assertThat(contentCreatorType).hasNoNullFieldsOrProperties();

        contentCreatorType.setName("Custom Creator Type");
        contentCreatorType.setDescription("Custom Creator Type");

        //then
        assertThat(repository.save(contentCreatorType))
                .isNotNull()
                .hasNoNullFieldsOrProperties()
                .hasFieldOrPropertyWithValue("name", "Custom Creator Type")
                .hasFieldOrPropertyWithValue("description", "Custom Creator Type");
    }

    @Test
    @Override
    void givenId_shouldDeleteEntity() {
        //given
        Long id = 3l;

        //when
        repository.deleteById(id);

        //then
        assertThat(repository.findById(id)).isEmpty();
    }

    @Test
    @Override
    void shouldListAllEntities() {
        var contentCreatorTypes = repository.findAll();
        assertThat(contentCreatorTypes).isNotEmpty().hasSize(4);
    }
}
