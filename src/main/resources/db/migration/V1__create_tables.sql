--DROP TABLE tb_categoria;

CREATE TABLE tb_categoria
(
    id bigint NOT NULL,
    nome character varying(255) NOT NULL,
    descricao text,
    CONSTRAINT tb_categoria_pkey PRIMARY KEY (id)
);

--ALTER TABLE tb_categoria DROP CONSTRAINT tb_categoria_pkey;

--ALTER TABLE tb_categoria
--    ADD CONSTRAINT tb_categoria_pkey PRIMARY KEY (id);

--DROP TABLE tb_tipo_criador;

CREATE TABLE tb_tipo_criador
(
    id bigint NOT NULL,
    nome character varying(255) NOT NULL,
    descricao text,
    CONSTRAINT tb_tipo_criador_pkey PRIMARY KEY (id)
);

--ALTER TABLE tb_tipo_criador DROP CONSTRAINT tb_tipo_criador_pkey;

--ALTER TABLE tb_tipo_criador
--    ADD CONSTRAINT tb_tipo_criador_pkey PRIMARY KEY (id);

--DROP TABLE tb_criador_conteudo;

CREATE TABLE tb_criador_conteudo
(
    id bigint NOT NULL,
    tipo_id bigint NOT NULL,
    primeiro_nome character varying(55) NOT NULL,
    ultimo_nome character varying(255) NULL,
    descricao text,
    CONSTRAINT tb_criador_conteudo_pkey PRIMARY KEY (id)
);

--ALTER TABLE tb_criador_conteudo DROP CONSTRAINT tb_criador_conteudo_pkey;

--ALTER TABLE tb_criador_conteudo
--    ADD CONSTRAINT tb_criador_conteudo_pkey PRIMARY KEY (id);

--ALTER TABLE tb_criador_conteudo DROP CONSTRAINT FK_tbcriadorconteudo_has_tipo;

ALTER TABLE tb_criador_conteudo
	ADD CONSTRAINT FK_tbcriadorconteudo_has_tipo FOREIGN KEY (tipo_id)
        REFERENCES tb_tipo_criador (id)
        ON UPDATE NO ACTION
        ON DELETE NO ACTION;

--DROP TABLE tb_formato_midia;

CREATE TABLE tb_formato_midia
(
    id bigint NOT NULL,
    nome character varying(255) NOT NULL,
    CONSTRAINT tb_formato_midia_pkey PRIMARY KEY (id)
);

--ALTER TABLE tb_formato_midia DROP CONSTRAINT tb_formato_midia_pkey;

--ALTER TABLE tb_formato_midia
--    ADD CONSTRAINT tb_formato_midia_pkey PRIMARY KEY (id);

--DROP TABLE tb_midia

CREATE TABLE tb_midia
(
    id bigint NOT NULL,
    formato_id bigint NOT NULL,
    tipo character varying(25) NOT NULL,
    titulo character varying(55) NOT NULL,
    subtitulo character varying(255) NULL,
    descricao text,
    dt_lancamento DATE,
    localizacao character varying(255),
    CONSTRAINT tb_midia_pkey PRIMARY KEY (id)
);

ALTER TABLE tb_midia
	ADD CONSTRAINT FK_tbmidia_has_formato FOREIGN KEY (formato_id)
        REFERENCES tb_formato_midia (id);

--DROP TABLE tb_midia_categoria

CREATE TABLE tb_midia_categoria
(
    midia_id bigint NOT NULL,
    categoria_id bigint NOT NULL,
    CONSTRAINT tb_midia_categoria PRIMARY KEY (midia_id, categoria_id)
);

ALTER TABLE tb_midia_categoria
	ADD CONSTRAINT FK_tb_midiacategoria_has_midia FOREIGN KEY (midia_id)
        REFERENCES tb_midia (id);

ALTER TABLE tb_midia_categoria
	ADD CONSTRAINT FK_tb_midiacategoria_has_categoria FOREIGN KEY (categoria_id)
        REFERENCES tb_categoria (id);

--DROP TABLE tb_midia_criador

CREATE TABLE tb_midia_criador
(
    midia_id bigint NOT NULL,
    criador_id bigint NOT NULL,
    CONSTRAINT tb_midia_criador PRIMARY KEY (midia_id, criador_id)
);

ALTER TABLE tb_midia_criador
	ADD CONSTRAINT FK_tb_midiacriador_has_midia FOREIGN KEY (midia_id)
        REFERENCES tb_midia (id);

ALTER TABLE tb_midia_criador
	ADD CONSTRAINT FK_tb_midiacriador_has_criador FOREIGN KEY (criador_id)
        REFERENCES tb_criador_conteudo (id);