insert into tb_categoria (id, nome) values (1,'Ação');
insert into tb_categoria (id, nome) values (2,'Aventura');
insert into tb_categoria (id, nome) values (3,'Biografia');
insert into tb_categoria (id, nome) values (4,'Comédia');
insert into tb_categoria (id, nome) values (5,'Drama');
insert into tb_categoria (id, nome) values (6,'Ficção científica');

insert into tb_formato_midia (id, nome) values (1,'Artigo');
insert into tb_formato_midia (id, nome) values (2,'Bluray/CD/DVD');
insert into tb_formato_midia (id, nome) values (3,'E-book');
insert into tb_formato_midia (id, nome) values (4,'Filme');
insert into tb_formato_midia (id, nome) values (5,'Livro');
insert into tb_formato_midia (id, nome) values (6,'Poema/Poesia');

insert into tb_tipo_criador (id, nome) values (1,'Autor(a)');
insert into tb_tipo_criador (id, nome) values (2,'Ator/Atriz');
insert into tb_tipo_criador (id, nome) values (3,'Diretor(a)');
insert into tb_tipo_criador (id, nome) values (4,'Editora');
insert into tb_tipo_criador (id, nome) values (5,'Estúdio');
insert into tb_tipo_criador (id, nome) values (6,'Gravadora');
insert into tb_tipo_criador (id, nome) values (7,'Poeta');

insert into tb_criador_conteudo (id, tipo_id, primeiro_nome, ultimo_nome) values (1, 1,'George', 'R. R. Martin');
insert into tb_criador_conteudo (id, tipo_id, primeiro_nome, ultimo_nome) values (2, 2,'George', 'Clooney');
insert into tb_criador_conteudo (id, tipo_id, primeiro_nome, ultimo_nome) values (3, 2,'Keanu', 'Reeves');
insert into tb_criador_conteudo (id, tipo_id, primeiro_nome, ultimo_nome) values (4, 2,'Jane', 'Fonda');
insert into tb_criador_conteudo (id, tipo_id, primeiro_nome, ultimo_nome) values (5, 3,'Quentin', 'Tarantino');
insert into tb_criador_conteudo (id, tipo_id, primeiro_nome, ultimo_nome) values (6, 4,'Sextante', null);
insert into tb_criador_conteudo (id, tipo_id, primeiro_nome, ultimo_nome) values (7, 5,'Fox', null);
insert into tb_criador_conteudo (id, tipo_id, primeiro_nome, ultimo_nome) values (8, 6,'Sony', null);
insert into tb_criador_conteudo (id, tipo_id, primeiro_nome, ultimo_nome) values (9, 7,'Machado', 'de Assis');