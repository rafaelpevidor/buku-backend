/**
 *
 */
package com.psystems.buku.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.psystems.buku.model.Category;

/**
 * @author rafaelpevidor
 *
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

}
