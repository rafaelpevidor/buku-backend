/**
 *
 */
package com.psystems.buku.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.psystems.buku.model.ContentCreatorType;

/**
 * @author rafaelpevidor
 *
 */
@Repository
public interface ContentCreatorTypeRepository extends JpaRepository<ContentCreatorType, Long> {

}
