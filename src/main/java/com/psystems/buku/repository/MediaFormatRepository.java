/**
 *
 */
package com.psystems.buku.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.psystems.buku.model.MediaFormat;

/**
 * @author rafaelpevidor
 *
 */
@Repository
public interface MediaFormatRepository extends JpaRepository<MediaFormat, Long> {

}
