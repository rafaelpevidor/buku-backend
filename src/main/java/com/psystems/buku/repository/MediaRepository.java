/**
 *
 */
package com.psystems.buku.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.psystems.buku.model.Media;
import com.psystems.buku.model.dto.SimpleMediaDTO;

/**
 * @author rafaelpevidor
 *
 */
@Repository
public interface MediaRepository extends JpaRepository<Media, Long> {

	@Query("SELECT new com.psystems.buku.model.dto.SimpleMediaDTO(obj.id AS id, obj.title AS title, obj.releaseDate AS releaseDate, obj.type AS type, format.name AS format) "
			+ "FROM Media obj INNER JOIN obj.format format")
	Page<SimpleMediaDTO> findAllLazy(Pageable pageable);
}
