/**
 *
 */
package com.psystems.buku.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.psystems.buku.model.ContentCreator;

/**
 * @author rafaelpevidor
 *
 */
@Repository
public interface ContentCreatorRepository extends JpaRepository<ContentCreator, Long> {

}
