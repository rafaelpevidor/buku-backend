/**
 *
 */
package com.psystems.buku.infra.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author rafaelpevidor
 *
 */
@Configuration
public class OpenAPIConfig {

    @Bean
    public OpenAPI openAPI() {
        return new OpenAPI().info(apiInfo());
    }

    private Info apiInfo() {
        return new Info()
                .title("Buku App")
                .description("Aplicativo para gestão de mídias, controle de empréstimo e avaliação (review).")
                .version("0.0.1");
    }
}
