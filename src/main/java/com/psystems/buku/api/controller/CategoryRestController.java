/**
 *
 */
package com.psystems.buku.api.controller;

import com.psystems.buku.model.Category;
import com.psystems.buku.repository.CategoryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Optional;

/**
 * @author rafaelpevidor
 *
 */
@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/api/categories")
public class CategoryRestController extends AbstractCrudController<Category, Long> {

	@Autowired
	private CategoryRepository repository;

	@GetMapping
	@Override
	public ResponseEntity<Page<Category>> getAll(Pageable pageable) {
		Page<Category> resultPage = repository.findAll(pageable);
		if (resultPage.hasContent())
			return new ResponseEntity<>(resultPage, HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/{id}")
	@Override
	public ResponseEntity<Category> get(@PathVariable("id") Long id) {
		Optional<Category> optCategory = repository.findById(id);
		if (optCategory.isPresent())
			return new ResponseEntity<>(optCategory.get(), HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PostMapping
	@Override
	public ResponseEntity<Category> create(@NotNull @Valid @RequestBody Category entity) {
		if (null != entity.getId()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(repository.save(entity), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@Override
	public ResponseEntity<Category> update(@PathVariable("id") Long id, @RequestBody @NotNull @Valid Category entity) {
		if (null != entity.getId() && (!id.equals(entity.getId()))) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
		return new ResponseEntity<>(repository.save(entity), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	@Override
	public ResponseEntity<Category> delete(@PathVariable("id") Long id) {
		Optional<Category> optCategory = repository.findById(id);
		if (optCategory.isPresent()) {
			repository.delete(optCategory.get());
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
