/**
 *
 */
package com.psystems.buku.api.controller;

import com.psystems.buku.model.ContentCreator;
import com.psystems.buku.repository.ContentCreatorRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import java.util.Optional;

/**
 * @author rafaelpevidor
 *
 */
@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/content-creators")
public class ContentCreatorRestController extends AbstractCrudController<ContentCreator, Long> {

	private final ContentCreatorRepository repository;

	@GetMapping
	@Override
	public ResponseEntity<Page<ContentCreator>> getAll(Pageable pageable) {
		Page<ContentCreator> resultPage = repository.findAll(pageable);
		if (resultPage.hasContent())
			return new ResponseEntity<>(resultPage, HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/{id}")
	@Override
	public ResponseEntity<ContentCreator> get(@PathVariable("id") Long id) {
		Optional<ContentCreator> optCreator = repository.findById(id);
		if (optCreator.isPresent())
			return new ResponseEntity<>(optCreator.get(), HttpStatus.OK);
		else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@PostMapping
	@Override
	public ResponseEntity<ContentCreator> create(@NotNull @Valid @RequestBody ContentCreator entity) {
		if (null != entity.getId()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(repository.save(entity), HttpStatus.CREATED);
	}

	@PutMapping("/{id}")
	@Override
	public ResponseEntity<ContentCreator> update(@PathVariable("id") Long id, @RequestBody @NotNull @Valid ContentCreator entity) {
		if (null != entity.getId() && (!id.equals(entity.getId()))) {
			return new ResponseEntity<>(HttpStatus.CONFLICT);
		}
		return new ResponseEntity<>(repository.save(entity), HttpStatus.OK);
	}

	@DeleteMapping("/{id}")
	@Override
	public ResponseEntity<ContentCreator> delete(@PathVariable("id") Long id) {
		Optional<ContentCreator> optCreator = repository.findById(id);
		if (optCreator.isPresent()) {
			repository.delete(optCreator.get());
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
