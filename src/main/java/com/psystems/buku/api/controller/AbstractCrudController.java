/**
 *
 */
package com.psystems.buku.api.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

/**
 * @author rafaelpevidor
 *
 */
public abstract class AbstractCrudController<T, I> {

	public abstract ResponseEntity<Page<T>> getAll(Pageable pageable);

	public abstract ResponseEntity<T> get(I id);

	public abstract ResponseEntity<T> create(T entity);

	public abstract ResponseEntity<T> update(I id, T entity);

	public abstract ResponseEntity<T> delete(I id);

}
