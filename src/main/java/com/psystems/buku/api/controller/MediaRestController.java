/**
 *
 */
package com.psystems.buku.api.controller;

import java.util.Collection;
import java.util.Optional;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.psystems.buku.model.Category;
import com.psystems.buku.model.Media;
import com.psystems.buku.model.dto.PartialUpdateMediaDTO;
import com.psystems.buku.model.dto.SimpleMediaDTO;
import com.psystems.buku.repository.MediaRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * @author rafaelpevidor
 *
 */
@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/medias")
public class MediaRestController {

	private final MediaRepository repository;

	@GetMapping
	public ResponseEntity<Page<SimpleMediaDTO>> getAll(Pageable pageable) {
		try {
			Page<SimpleMediaDTO> resultPage = repository.findAllLazy(pageable);
			if (resultPage.hasContent())
				return new ResponseEntity<>(resultPage, HttpStatus.OK);
			else
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<Media> get(@PathVariable("id") Long id) {
		try {
			Optional<Media> optMedia = repository.findById(id);
			if (optMedia.isPresent())
				return new ResponseEntity<>(optMedia.get(), HttpStatus.OK);
			else
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	@PostMapping
	public ResponseEntity<Media> create(@NotNull @Valid @RequestBody Media entity) {
		try {
			if (null != entity.getId()) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			return new ResponseEntity<>(repository.save(entity), HttpStatus.CREATED);
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	@PatchMapping("/{id}")
	public ResponseEntity<Media> patch(@PathVariable("id") Long id, @RequestBody @NotNull @Valid PartialUpdateMediaDTO dto) {
		try {
			Optional<Media> optMedia = repository.findById(id);
			if (optMedia.isPresent()) {
				Media entity = optMedia.get();
				entity.setLocation(dto.getLocation());
				entity.setReleaseDate(dto.getReleaseDate());
				entity.setSubtitle(dto.getSubtitle());
				entity.setTitle(dto.getTitle());
				entity.setSummary(dto.getSummary());

				updateCategories(dto.getCategories(), entity);

				return new ResponseEntity<>(repository.save(entity), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	private void updateCategories(Collection<Category> dtoCategories, Media entity) {
		entity.getCategories().clear();
		for (Category category : dtoCategories) {
			entity.addCategory(category);
		}
//		Collection<Category> categories = new ArrayList<>();
//		categories.addAll(entity.getCategories());
//
//		for (Category category : categories) {
//			if (!dtoCategories.contains(category)) {
//				entity.removeCategory(category);
//			}
//		}
//
//		for (Category category : dtoCategories) {
//			if (!entity.getCategories().contains(category)) {
//				entity.addCategory(category);
//			}
//		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<Media> update(@PathVariable("id") Long id, @RequestBody @NotNull @Valid Media entity) {
		try {
			if (null != entity.getId() && (!id.equals(entity.getId()))) {
				return new ResponseEntity<>(HttpStatus.CONFLICT);
			}

			Optional<Media> optMedia = repository.findById(id);
			if (optMedia.isPresent()) {
				updateCategories(optMedia.get().getCategories(), entity);
				return new ResponseEntity<>(repository.save(entity), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			log.error(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Media> delete(@PathVariable("id") Long id) {
		try {
			Optional<Media> optMedia = repository.findById(id);
			if (optMedia.isPresent()) {
				repository.delete(optMedia.get());
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}

}
