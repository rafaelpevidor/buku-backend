/**
 *
 */
package com.psystems.buku.model.dto;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.psystems.buku.model.Category;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Collection;

/**
 * @author rafaelpevidor
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class PartialUpdateMediaDTO implements Serializable {

	private static final long serialVersionUID = -6729243035921875580L;

	@Size(min = 3, max = 50, message = "título deve ter no mínimo 3 e no máximo 50 caracteres.")
	@NotBlank(message = "título é um campo obrigatório.")
	private String title;

	@Size(max = 200, message = "subtítulo deve ter no máximo 200 caracteres.")
	private String subtitle;

	@Size(max = 200, message = "localização deve ter no máximo 200 caracteres.")
	private String location;

	@Size(max = 600, message = "resumo deve ter no máximo 600 caracteres.")
	private String summary;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	private LocalDate releaseDate;

	@Size(min = 1, message = "uma mídia deve ter no mínimo uma categoria.")
	private Collection<Category> categories;
}
