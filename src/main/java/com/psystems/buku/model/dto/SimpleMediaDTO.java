/**
 *
 */
package com.psystems.buku.model.dto;

import java.io.Serializable;
import java.time.LocalDate;

import com.psystems.buku.model.MediaType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author rafaelpevidor
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class SimpleMediaDTO implements Serializable {

	private static final long serialVersionUID = -2479174973060957229L;

	private Long id;
	private String title;
	private LocalDate releaseDate;
	private MediaType type;
	private String format;
}
