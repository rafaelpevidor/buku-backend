/**
 *
 */
package com.psystems.buku.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * @author rafaelpevidor
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@Entity
@Table(name = "tb_midia")
public class Media implements Serializable {

	private static final long serialVersionUID = 7775388757057529198L;

	@Id
	@GeneratedValue(generator = "mediaIdGenerator")
	@GenericGenerator(name = "mediaIdGenerator", strategy = "increment")
	@Column(updatable = false, nullable = false)
	private Long id;

	@OrderBy
	@Size(min = 3, max = 50, message = "título deve ter no mínimo 3 e no máximo 50 caracteres.")
	@NotBlank(message = "título é um campo obrigatório.")
	@Column(name = "titulo", nullable = false)
	private String title;

	@Size(max = 200, message = "subtítulo deve ter no máximo 200 caracteres.")
	@Column(name = "subtitulo")
	private String subtitle;

	@JsonDeserialize(using = LocalDateDeserializer.class)
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonFormat(shape = Shape.STRING, pattern = "dd/MM/yyyy")
	@Column(name = "dt_lancamento")
	private LocalDate releaseDate;

	@Size(max = 600, message = "resumo deve ter no máximo 600 caracteres.")
	@Column(name = "descricao")
	private String summary;

	@Size(max = 200, message = "localizacao deve ter no máximo 200 caracteres.")
	@Column(name = "localizacao")
	private String location;

	@Transient
	private String sharedUrl;

	@NotNull(message = "tipo é um campo obrigatório.")
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo", nullable = false)
	private MediaType type;

	@NotNull(message = "formato é um campo obrigatório.")
	@ManyToOne
	@JoinColumn(name = "formato_id", nullable = false)
	private MediaFormat format;

	@Size(min = 1, message = "uma mídia deve ter no mínimo um criador/autor/responsável.")
	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE})
	@JoinTable(name = "tb_midia_criador",
    joinColumns = @JoinColumn(name = "midia_id"),
    inverseJoinColumns = @JoinColumn(name = "criador_id"))
	private Set<ContentCreator> contentCreators = new HashSet<>();

	@Size(min = 1, message = "uma mídia deve ter no mínimo uma categoria.")
	@ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.REMOVE})
	@JoinTable(name = "tb_midia_categoria",
	    joinColumns = @JoinColumn(name = "midia_id"),
	    inverseJoinColumns = @JoinColumn(name = "categoria_id"))
	private Set<Category> categories = new HashSet<>();

	public void addCategory(Category category) {
		this.categories.add(category);
	}

	public void removeCategory(Category category) {
		this.categories.remove(category);
	}

	public void addContentCreator(ContentCreator contentCreator) {
		this.contentCreators.add(contentCreator);
	}

	public void renameContentCreator(ContentCreator contentCreator) {
		this.contentCreators.remove(contentCreator);
	}

}
