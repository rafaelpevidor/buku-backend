/**
 *
 */
package com.psystems.buku.model;

/**
 * @author rafaelpevidor
 *
 */
public enum MediaType {

	DIGITAL("Digital"),
	PHYSICAL("Física");

	/**
	 * @param label
	 */
	private MediaType(String label) {
		this.label = label;
	}

	private String label;

	public String getLabel() {
		return label;
	}
}
