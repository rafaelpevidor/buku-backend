/**
 *
 */
package com.psystems.buku.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OrderBy;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author rafaelpevidor
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Data//is a convenient shortcut annotation that bundles the features of @ToString, @EqualsAndHashCode, @Getter / @Setter and @RequiredArgsConstructor together
@Builder
@Entity
@Table(name = "tb_tipo_criador")
public class ContentCreatorType implements Serializable {

	private static final long serialVersionUID = 2273436049554572887L;

	@Id
	@GeneratedValue(generator = "makerTypeIdGenerator")
	@GenericGenerator(name = "makerTypeIdGenerator", strategy = "increment")
	@Column(updatable = false, nullable = false)
	private Long id;

	@OrderBy(clause = "name asc")
	@Size(min = 3, max = 50, message = "nome deve ter no mínimo 3 e no máximo 50 caracteres.")
	@NotBlank(message = "nome é um campo obrigatório.")
	@Column(name = "nome", nullable = false)
	private String name;

	@Size(max = 600, message = "descrição deve ter no máximo 600 caracteres.")
	@Column(name = "descricao")
	private String description;

}
