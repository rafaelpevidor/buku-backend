/**
 *
 */
package com.psystems.buku.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import java.io.Serializable;

/**
 * @author rafaelpevidor
 *
 */
@AllArgsConstructor
@NoArgsConstructor
@Data//is a convenient shortcut annotation that bundles the features of @ToString, @EqualsAndHashCode, @Getter / @Setter and @RequiredArgsConstructor together
@Builder
@Entity
@Table(name = "tb_criador_conteudo")
public class ContentCreator implements Serializable {

	private static final long serialVersionUID = 8487307698725722149L;

	@Id
	@GeneratedValue(generator = "creatorIdGenerator")
	@GenericGenerator(name = "creatorIdGenerator", strategy = "increment")
	@Column(updatable = false, nullable = false)
	private Long id;

	@Column(name = "primeiro_nome", nullable = false)
	private String firstName;

	@Column(name = "ultimo_nome", nullable = false)
	private String lastName;

	@Column(name = "descricao")
	private String summary;

	@ManyToOne
	@JoinColumn(name = "tipo_id", nullable = false)
	private ContentCreatorType type;

}
